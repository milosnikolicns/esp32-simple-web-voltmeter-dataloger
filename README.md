# esp32-simple-web-voltmeter-dataloger



------



 # Motivation

- Make simple and practical data logger 
- Live graph data to simplify its understanding
- Stack sampled data to html table which can be copied to excel spreadsheet.

- Use multiple technologies to their best advantage

  

------



 #  Project 

- Arduino code (AD data sampling code and emended web backend).
- Html frontend (Graph and table of data)

------

![](https://storage.googleapis.com/my-bucket-permanent/meta/Screenshot%202021-01-18%20120601.jpg)



